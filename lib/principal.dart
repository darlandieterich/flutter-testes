import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:testes/pagina.dart' as page;
import 'package:testes/image.dart' as img;
import 'package:testes/tabela.dart' as tbla;
import 'package:testes/drawer.dart' as drw;
import 'package:testes/drag_drop.dart' as drgdrp;

class PaginaPrincipal extends StatefulWidget {

  PaginaPrincipal({Key key, this.title}) : super(key: key);

  final String title;

  @override
  PricipalMain createState() => new PricipalMain();
}

class PricipalMain extends State<PaginaPrincipal> {
  int _counter = 0;
  String _down = ''; 
  bool loading = false;
  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }
  void _decrement(){
    setState(() {
      _counter--; 
    });
  }
  Future<String> download() async {
      loading = true;
      setState(() {
        _down = 'Carregando..'; 
      });
      var res = await http.get('https://reqres.in/api/users?page=2');
      print(res.body);
      setState(() {
        _down = res.body;
      });
      loading = false;
    return "Sucesso!!";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            loading ? new CircularProgressIndicator() : new Container(),
            Text(
              'Pressione (+) ou (-):',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
            Text(
              '$_down',
              style: Theme.of(context).textTheme.body1,
            ),
            new Row(
              children: <Widget>[
                FloatingActionButton(
                  heroTag: null,
                  tooltip: 'DragDrop',
                  child: Icon(Icons.adjust),              
                  onPressed: () {
                    Route route = MaterialPageRoute(builder: (context) => new drgdrp.PaginaDragDrop());
                    Navigator.of(context).push(route);
                  }              
                )
              ],
            )
          ],
        ),
      ),
      floatingActionButton: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            FloatingActionButton(
              heroTag: null,
              onPressed: _decrement,
              tooltip: 'Decrementa',
              child: Icon(Icons.remove),
            ),
            FloatingActionButton(
              heroTag: null,
              onPressed: _incrementCounter,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
            FloatingActionButton(
              heroTag: null,
              onPressed: download,
              tooltip: 'Download',
              child: Icon(Icons.file_download),
            ),
            FloatingActionButton(
              heroTag: null,
              tooltip: 'Outra página',
              child: Icon(Icons.navigate_next),              
              onPressed: () {
                Route route = MaterialPageRoute(builder: (context) => new page.OutraPagina());
                Navigator.of(context).push(route);
              }              
            ),
            FloatingActionButton(
              heroTag: null,
              tooltip: 'Imagem',
              child: Icon(Icons.image),              
              onPressed: () {
                Route route = MaterialPageRoute(builder: (context) => new img.PaginaImagem());
                Navigator.of(context).push(route);
              }              
            ),
            FloatingActionButton(
              heroTag: null,
              tooltip: 'Tabela',
              child: Icon(Icons.table_chart),              
              onPressed: () {
                Route route = MaterialPageRoute(builder: (context) => new tbla.PaginaTabela());
                Navigator.of(context).push(route);
              }              
            ),
            FloatingActionButton(
              heroTag: null,
              tooltip: 'Menu',
              child: Icon(Icons.menu),              
              onPressed: () {
                Route route = MaterialPageRoute(builder: (context) => new drw.PaginaDrawer());
                Navigator.of(context).push(route);
              }              
            ),
            
          ],
      ),
      
    );
  }
}
