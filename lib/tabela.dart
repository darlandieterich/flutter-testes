import 'package:flutter/material.dart';
import 'dart:convert';

class PaginaTabela extends StatefulWidget {
  PaginaTabela({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Tabela createState() => new Tabela();
}

class Tabela extends State<PaginaTabela> {
  final List<Map<String, String>> listOfColumns = [
    {"Nome": "Darlan", "Idade": "30"},
    {"Nome": "Laura", "Idade": "6"},
    {"Nome": "Jenifer", "Idade": "30"}
  ];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text("Tabela"),
          backgroundColor: Colors.lime,
        ),
        body: new Center(
          child: new Column(
            children: <Widget>[              
              new Container(
                margin: EdgeInsets.only(top: 20),
                child: CircleAvatar(
                  backgroundColor: Colors.orangeAccent,
                  backgroundImage:
                      new AssetImage("assets/img/simpsons-family.jpg"),
                  maxRadius: 100,
                ),
              ),
              Divider(),
              Text('Tabela Familiar'),
              new Container(
                child: DataTable(
                  columns: [
                    DataColumn(label: Text('Nome')),
                    DataColumn(label: Text('Idade'))
                  ],
                  rows:
                      listOfColumns // Loops through dataColumnText, each iteration assigning the value to element
                          .map(
                            ((element) => DataRow(
                                  cells: <DataCell>[
                                    DataCell(Text(element["Nome"])), //Extracting from Map element the value
                                    DataCell(Text(element["Idade"]))
                                  ],
                                )),
                          )
                          .toList(),
                )     
              ),
              new Container(
                margin: EdgeInsets.only(top: 40),
                child: FloatingActionButton(
                  heroTag: null,
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  tooltip: 'Voltar',
                  child: Icon(Icons.arrow_back),
                ),
              ),
            ],
          ),
        ));
  }
}
