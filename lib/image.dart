import 'package:flutter/material.dart';
import 'dart:convert';

class PaginaImagem extends StatefulWidget {

  PaginaImagem({Key key, this.title}) : super(key: key);

  static const String routeName = "/PaginaImagem";

  final String title;

  @override
  Imagem createState() => new Imagem();
}

class Imagem extends State<PaginaImagem> {

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Imagemmm"), backgroundColor: Colors.teal,
      ),
      body: new Center(
        child: new Column(                   
          children: <Widget>[ 
            new Container(              
              child: FloatingActionButton(
                heroTag: null,
                onPressed: () {
                Navigator.pop(context);
                },
                tooltip: 'Voltar',
                child: Icon(Icons.arrow_back),
              ),      
            ), 
            new Container(
                margin: const EdgeInsets.all(10.0),
                height: 150,
                width: 200,
                decoration: BoxDecoration(
                  color: Colors.yellow,
                  image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: NetworkImage(
                      'https://flutter.io/images/catalog-widget-placeholder.png',
                    ),
                  ),
                ),
              ),
            new Container(
                margin: const EdgeInsets.all(10.0),
                height: 100,
                width: 200,
                decoration: BoxDecoration(
                  color: Colors.yellow,
                  border: Border.all(color: Colors.black, width: 3),
                  image: DecorationImage(
                    fit: BoxFit.fitWidth,
                    image: new AssetImage("assets/img/simpsons-family.jpg"),                    
                  ),
                ),
              ),
          ],
        ),
      )
      
    );
  }
}