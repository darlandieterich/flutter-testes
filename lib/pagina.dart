import 'package:flutter/material.dart';
import 'dart:convert';

class OutraPagina extends StatefulWidget {

  OutraPagina({Key key, this.title}) : super(key: key);

  final String title;

  @override
  SecondRoute createState() => new SecondRoute();
}

class SecondRoute extends State<OutraPagina> {
  List data = new List();
  
  void getData () {
      data.add({"valor": "1111"});
      data.add({"valor": "2222"});
  }

  @override
  void initState(){
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text("Segunda tela"), backgroundColor: Colors.cyan,
      ),
      body: new Center(
        child: new Column(                   
          children: <Widget>[ 
            new Container(
              child: FloatingActionButton(
                heroTag: null,
                onPressed: () {
                Navigator.pop(context);
                },
                tooltip: 'Voltar',
                child: Icon(Icons.arrow_back),
              ),       

            ),
            Expanded(
              child: ListView.builder(
                itemCount: data == null ? 0 : data.length,
                itemBuilder: (BuildContext context, int index){
                  return new Card(
                    child: new Text(data[index]["valor"]),
                  );
                },                
              ),
            ),
             
          ],
        ),
      )
      
    );
  }
}