import 'package:flutter/material.dart';
import 'dart:convert';

class PaginaDrawer extends StatefulWidget {
  PaginaDrawer({Key key, this.title}) : super(key: key);

  final String title;

  @override
  TipoDrawer createState() => new TipoDrawer();
}

class TipoDrawer extends State<PaginaDrawer> {
  int _index = 0;

  Widget _switcherBody() {
    switch (_index) {
      case 0:
        return new Container(child: Center(
          child: new Column(
            children: <Widget>[
              new Container(
                margin: EdgeInsets.all(10),
                child: new Text("Tela 2"),
              ),
              new FloatingActionButton(
                heroTag: null,
                onPressed: () {
                Navigator.pop(context);
                },
                tooltip: 'Voltar',
                child: Icon(Icons.arrow_back),
              ),
            ],
          )
        ));
      case 1:
        return new Container(child: Center(child: new Text("Tela 2")));
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Drawer")),
      body: _switcherBody(),
      drawer: new Drawer(
        child: new ListView(padding: EdgeInsets.zero, children: <Widget>[
          DrawerHeader(
            child: Text('Drawer titulo'),
            decoration: BoxDecoration(
              color: Colors.yellow,
            ),
          ),
          ListTile(
            title: Text('Tela 1'),
            onTap: () {
              setState(() {
                _index = 0;
              });
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Tela 2'),
            onTap: () {
              setState(() {
                _index = 1;
              });
              Navigator.pop(context);
            },
          ),
        ]),
      ),
    );
  }
}
